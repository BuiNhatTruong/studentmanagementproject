﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DatabaseAccessLayer;
using ValueObject;
using System.Data.SqlClient;
namespace BussinessAccessLayer
{
    public class BAL_Admin
    {
        private static DA_Admin da_Admin = new DA_Admin();
       

        public DataTable GetAllAdminInfo()
        {
            return da_Admin.GetAllAdminInfo();
        }

        public DataTable SearchingAdminInfo(string keyWords)
        {
            return da_Admin.SearchingAdminInfo(keyWords);
        }

        public int UpdateAdminPassWord(string adminId, string oldPassword, string newPassword)
        {
            return da_Admin.UpdateAdminPassword(adminId, oldPassword, newPassword);
        }

        public int UpdateAdminProfile(Admin admin)
        {
            return da_Admin.UpdateAdminProfile(admin);
        }

        public int AddAdmin(Admin admin)
        {
            return da_Admin.AddAdmin(admin);
        }

        public int DeleteAdmin(Admin admin)
        {
            return da_Admin.DeleteAdmin(admin);
        }

    }
}
