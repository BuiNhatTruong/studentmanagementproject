﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValueObject;
using System.Data.SqlClient;
using System.Data;

namespace DatabaseAccessLayer
{
    public class AdminDatabaseAccess
    {
        DatabaseAccess databaseAccess = new DatabaseAccess();

        public DataTable GetAllAdminData(Admin admin)
        {
            SqlParameter[] para =
                {
                    new SqlParameter("adminId", admin.AdminId)
                };
            return databaseAccess.GetData("proc_SelectAllAdmin", para);
        }
    }
}
