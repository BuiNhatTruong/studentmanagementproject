﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace DatabaseAccessLayer
{
    public class DatabaseAccess
    {
        public SqlConnection sqlConnection;

        public DatabaseAccess()
        {
            
        }

        public DataTable GetData(string procName,SqlParameter[] para)
        {
            using (sqlConnection = new SqlConnection(@"Data Source=WINDOWS-QJFDAQQ\SQLEXPRESS;Initial Catalog=QuanLySinhVien;Integrated Security=True"))
            {
                DataTable dataTable = new DataTable();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                SqlCommand sqlCommand = new SqlCommand(procName, sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                if (para != null)
                {
                    sqlCommand.Parameters.AddRange(para);
                }
                sqlConnection.Open();
                dataAdapter.SelectCommand = sqlCommand;
                dataAdapter.Fill(dataTable);
                sqlConnection.Close();
                return dataTable;
            }
            
        }


        public int ExecuteQuery(string procName,SqlParameter[] para)
        {
            using (sqlConnection = new SqlConnection(@"Data Source=WINDOWS-QJFDAQQ\SQLEXPRESS;Initial Catalog=QuanLySinhVien;Integrated Security=True"))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(procName, sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                if (para != null)
                {
                    sqlCommand.Parameters.AddRange(para);
                }
                int result = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                return result;
            }
        }

        public int ExecuteQuery(string queryString)
        {
            using (sqlConnection = new SqlConnection(@"Data Source=WINDOWS-QJFDAQQ\SQLEXPRESS;Initial Catalog=QuanLySinhVien;Integrated Security=True"))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(queryString, sqlConnection);
                sqlCommand.CommandType = CommandType.Text;
                int result = sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                return result;
            }
        }
    }
}
