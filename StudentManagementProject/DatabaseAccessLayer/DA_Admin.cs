﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using ValueObject;

namespace DatabaseAccessLayer
{
    public class DA_Admin
    {
        DatabaseAccess databaseAccess = new DatabaseAccess();

        public DataTable GetAllAdminInfo()
        {
            return databaseAccess.GetData("proc_AdminSelectAll", null);
        }

        public DataTable SearchingAdminInfo(string keyWords)
        {
            SqlParameter[] para =
            {
                new SqlParameter("@_AdminRequest",keyWords)
            };
            return databaseAccess.GetData("proc_SearchingAdmin", para);
        }

        public int UpdateAdminPassword(string adminId, string oldPassword, string newPassword)
        {
            string queryString = "UPDATE AdminTable SET adminPassword=HASHBYTES('MD5','" + newPassword +
                "') WHERE adminId='" + adminId + "' COLLATE SQL_Latin1_General_CP1_CS_AS AND adminPassword=HASHBYTES('MD5','" + oldPassword + "')";
            return databaseAccess.ExecuteQuery(queryString);
        }

        public int UpdateAdminProfile(Admin admin)
        {
            SqlParameter[] para =
            {
                new SqlParameter("@_adminId",admin.AdminId),
                new SqlParameter("@_adminName",admin.AdminName),
                new SqlParameter("@_adminBirth",admin.AdminBirth),
                new SqlParameter("@_adminEmail",admin.AdminEmail),
                new SqlParameter("@_adminAddress",admin.AdminAddress)
            };
            return databaseAccess.ExecuteQuery("proc_UpdateAdminProfile",para);
        }

        public int AddAdmin(Admin admin)
        {
            SqlParameter[] para =
            {
                new SqlParameter("@_adminId",admin.AdminId),
                new SqlParameter("@_adminPassword",admin.AdminPassword),
                new SqlParameter("@_adminName",admin.AdminName),
                new SqlParameter("@_adminType",admin.AdminType),
                new SqlParameter("@_adminBirth",admin.AdminBirth),
                new SqlParameter("@_adminAddress",admin.AdminAddress),
                new SqlParameter("@_adminEmail",admin.AdminEmail),
                new SqlParameter("@_adminOnlineHour",admin.AdminOnlineHour),
            };
            return databaseAccess.ExecuteQuery("proc_InsertAdmin",para);
        }

        public int DeleteAdmin(Admin admin)
        {
            SqlParameter[] para =
            {
                new SqlParameter("@_adminId",admin.AdminId)
            };
            return databaseAccess.ExecuteQuery("proc_DeleteAdmin",para);
        }

    }
}
