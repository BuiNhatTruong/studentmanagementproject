﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace ValueObject
{
    public class Admin
    {
        private string adminId;
        private string adminPassword;
        private string adminName;
        private int adminType;
        private DateTime adminBirth;
        private string adminAddress;
        private string adminEmail;
        private int adminOnlineHour;

        public string AdminId
        {
            get
            {
                return adminId;
            }

            set
            {
                adminId = value;
            }
        }

        public string AdminName
        {
            get
            {
                return adminName;
            }

            set
            {
                adminName = value;
            }
        }

        public int AdminType
        {
            get
            {
                return adminType;
            }

            set
            {
                adminType = value;
            }
        }

        public string AdminPassword
        {
            get
            {
                return adminPassword;
            }

            set
            {
                adminPassword = value;
            }
        }

        public DateTime AdminBirth
        {
            get
            {
                return adminBirth;
            }

            set
            {
                adminBirth = value;
            }
        }

        public string AdminAddress
        {
            get
            {
                return adminAddress;
            }

            set
            {
                adminAddress = value;
            }
        }

        public string AdminEmail
        {
            get
            {
                return adminEmail;
            }

            set
            {
                adminEmail = value;
            }
        }

        public int AdminOnlineHour
        {
            get
            {
                return adminOnlineHour;
            }

            set
            {
                adminOnlineHour = value;
            }
        }

        public Admin()
        {
        }

        public Admin(string id, string password, string name, int type, DateTime birth, string address, string email, int onlineHour)
        {
            AdminId = id;
            AdminPassword = password;
            AdminName = name;
            AdminType = type;
            AdminBirth = birth;
            AdminAddress = address;
            AdminEmail = email;
            AdminOnlineHour = onlineHour;
        }

    }
}
