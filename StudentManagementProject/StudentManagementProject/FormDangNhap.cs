﻿using DatabaseAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using ValueObject;

namespace StudentManagementProject
{
    public partial class FormDangNhap : Form
    {
        public static Admin admin = new Admin();
        DatabaseAccess databaseAccess = new DatabaseAccess();
        public FormDangNhap()
        {
            InitializeComponent();
            
        }

        

        private void FormDangNhap_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Bạn có thực sự muốn thoát","THÔNG BÁO",MessageBoxButtons.OKCancel,MessageBoxIcon.Information)!=DialogResult.OK)
            {
                e.Cancel = true;
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(textBoxID.Text)||string.IsNullOrEmpty(textBoxPassword.Text))
            {
                MessageBox.Show("Vui lòng điền đầy đủ thông tin","THÔNG BÁO",MessageBoxButtons.OK,MessageBoxIcon.Information);
                if(string.IsNullOrEmpty(textBoxID.Text))
                {
                    textBoxID.Focus();
                    return;
                }
                textBoxPassword.Focus();
                return;
            }
            using (SqlConnection connection = new SqlConnection(@"Data Source=WINDOWS-QJFDAQQ\SQLEXPRESS;Initial Catalog=QuanLySinhVien;Integrated Security=True"))
            {
                connection.Open();

                // Sử dụng COLLATE để phân biệt kí tự hoa thường trong CSDL 
                string sqlQuery = "SELECT COUNT(*) FROM AdminTable WHERE adminId=N'" + textBoxID.Text
                    + "' COLLATE SQL_Latin1_General_CP1_CS_AS AND adminPassword=HASHBYTES('MD5','" + textBoxPassword.Text + "')";
                SqlCommand sqlCommand = new SqlCommand(sqlQuery, connection);
                sqlCommand.CommandType = CommandType.Text;
                int loginResult = (int)sqlCommand.ExecuteScalar();
                if (loginResult == 1)
                {
                    MessageBox.Show("Đăng nhập thành công", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sqlQuery = "SELECT * FROM AdminTable WHERE adminId=N'" + textBoxID.Text
                        + "' COLLATE SQL_Latin1_General_CP1_CS_AS AND adminPassword=HASHBYTES('MD5','" + textBoxPassword.Text + "')";
                    sqlCommand = new SqlCommand(sqlQuery, connection);
                    sqlCommand.CommandType = CommandType.Text;
                    SqlDataReader dataReader = sqlCommand.ExecuteReader();
                    while (dataReader.Read())
                    {
                        admin.AdminId = dataReader["adminId"].ToString();
                        admin.AdminPassword = dataReader["adminPassword"].ToString();
                        admin.AdminName = dataReader["adminName"].ToString();
                        admin.AdminType = (int)dataReader["adminType"];
                        admin.AdminBirth = (DateTime)dataReader["adminBirth"];
                        admin.AdminEmail = dataReader["adminEmail"].ToString();
                        admin.AdminOnlineHour = (int)dataReader["adminOnlineHour"];
                        admin.AdminAddress = dataReader["adminAddress"].ToString();
                    }
                    FormMain formMain = new FormMain();
                    this.Hide();
                    formMain.ShowDialog();
                    this.Show();
                    textBoxID.Text = null;
                    textBoxPassword.Text = null;
                    textBoxID.Focus();
                }
                else
                {
                    MessageBox.Show("Đăng nhập thất bại! Vui lòng kiểm tra lại Id hoặc mật khẩu và thử lại", "ĐĂNG NHẬP THẤT BẠI", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                connection.Close();
            }
        }
    }
}
