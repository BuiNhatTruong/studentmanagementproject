﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementProject
{
    public partial class FormEmailFeedBack : Form
    {
        public FormEmailFeedBack()
        {
            InitializeComponent();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            
            try
            {
                new System.Net.Mail.MailAddress(textBoxEmail.Text);

                try
                {
                    MailMessage mailMessage = new MailMessage(textBoxEmail.Text, "buinhattruong1996@gmail.com", textBoxSubject.Text, richTextBoxContent.Text);
                    SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                    smtpClient.EnableSsl = true;
                    smtpClient.Credentials = new NetworkCredential(textBoxEmail.Text, textBoxPassword.Text);
                    smtpClient.Send(mailMessage);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Vui lòng bật chế độ \"less secure apps \" của tài khoản Gmail này");
                    MessageBox.Show(ex.ToString());
                }
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Chưa nhập địa chỉ email", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (FormatException)
            {
                MessageBox.Show("Địa chỉ email không hợp lệ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
