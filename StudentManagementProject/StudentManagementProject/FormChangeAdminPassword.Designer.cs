﻿namespace StudentManagementProject
{
    partial class FormChangeAdminPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxAccountInfo = new System.Windows.Forms.GroupBox();
            this.buttonChangePassword = new System.Windows.Forms.Button();
            this.buttonCancelUpdate = new System.Windows.Forms.Button();
            this.textBoxNewPassword = new System.Windows.Forms.TextBox();
            this.textBoxOldPassword = new System.Windows.Forms.TextBox();
            this.textBoxAdminId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxAccountInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxAccountInfo
            // 
            this.groupBoxAccountInfo.Controls.Add(this.buttonChangePassword);
            this.groupBoxAccountInfo.Controls.Add(this.buttonCancelUpdate);
            this.groupBoxAccountInfo.Controls.Add(this.textBoxNewPassword);
            this.groupBoxAccountInfo.Controls.Add(this.textBoxOldPassword);
            this.groupBoxAccountInfo.Controls.Add(this.textBoxAdminId);
            this.groupBoxAccountInfo.Controls.Add(this.label2);
            this.groupBoxAccountInfo.Controls.Add(this.label3);
            this.groupBoxAccountInfo.Controls.Add(this.label1);
            this.groupBoxAccountInfo.Location = new System.Drawing.Point(4, 12);
            this.groupBoxAccountInfo.Name = "groupBoxAccountInfo";
            this.groupBoxAccountInfo.Size = new System.Drawing.Size(280, 128);
            this.groupBoxAccountInfo.TabIndex = 0;
            this.groupBoxAccountInfo.TabStop = false;
            this.groupBoxAccountInfo.Text = "Thông tin tài khoản";
            // 
            // buttonChangePassword
            // 
            this.buttonChangePassword.Location = new System.Drawing.Point(114, 97);
            this.buttonChangePassword.Name = "buttonChangePassword";
            this.buttonChangePassword.Size = new System.Drawing.Size(75, 23);
            this.buttonChangePassword.TabIndex = 7;
            this.buttonChangePassword.Text = "Cập nhật";
            this.buttonChangePassword.UseVisualStyleBackColor = true;
            this.buttonChangePassword.Click += new System.EventHandler(this.buttonChangePassword_Click);
            // 
            // buttonCancelUpdate
            // 
            this.buttonCancelUpdate.Location = new System.Drawing.Point(195, 97);
            this.buttonCancelUpdate.Name = "buttonCancelUpdate";
            this.buttonCancelUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelUpdate.TabIndex = 6;
            this.buttonCancelUpdate.Text = "Hủy";
            this.buttonCancelUpdate.UseVisualStyleBackColor = true;
            this.buttonCancelUpdate.Click += new System.EventHandler(this.buttonCancelUpdate_Click);
            // 
            // textBoxNewPassword
            // 
            this.textBoxNewPassword.Location = new System.Drawing.Point(114, 71);
            this.textBoxNewPassword.Name = "textBoxNewPassword";
            this.textBoxNewPassword.Size = new System.Drawing.Size(156, 20);
            this.textBoxNewPassword.TabIndex = 5;
            this.textBoxNewPassword.UseSystemPasswordChar = true;
            // 
            // textBoxOldPassword
            // 
            this.textBoxOldPassword.Location = new System.Drawing.Point(114, 45);
            this.textBoxOldPassword.Name = "textBoxOldPassword";
            this.textBoxOldPassword.Size = new System.Drawing.Size(156, 20);
            this.textBoxOldPassword.TabIndex = 4;
            this.textBoxOldPassword.UseSystemPasswordChar = true;
            // 
            // textBoxAdminId
            // 
            this.textBoxAdminId.Location = new System.Drawing.Point(114, 19);
            this.textBoxAdminId.Name = "textBoxAdminId";
            this.textBoxAdminId.ReadOnly = true;
            this.textBoxAdminId.Size = new System.Drawing.Size(156, 20);
            this.textBoxAdminId.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "MẬT KHẨU CŨ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "MẬT KHẨU MỚI:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID:";
            // 
            // FormChangeAdminPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 144);
            this.Controls.Add(this.groupBoxAccountInfo);
            this.Name = "FormChangeAdminPassword";
            this.Text = "THAY ĐỔI MẬT KHẨU";
            this.groupBoxAccountInfo.ResumeLayout(false);
            this.groupBoxAccountInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxAccountInfo;
        private System.Windows.Forms.TextBox textBoxNewPassword;
        private System.Windows.Forms.TextBox textBoxOldPassword;
        private System.Windows.Forms.TextBox textBoxAdminId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonChangePassword;
        private System.Windows.Forms.Button buttonCancelUpdate;
    }
}