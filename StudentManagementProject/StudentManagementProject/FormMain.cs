﻿using System;
using DatabaseAccessLayer;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BussinessAccessLayer;

namespace StudentManagementProject
{
    public partial class FormMain : Form
    {
        BAL_Admin bal_Admin = new BAL_Admin();

        public FormMain()
        {
            // Just some stupid comment for bitbucket test
            InitializeComponent();
            
        }
        

        private string GetAdminName()
        {
            return FormDangNhap.admin.AdminName;
        }
        private bool CheckTypeOfAdmin()
        {
            if (FormDangNhap.admin.AdminType == 1)
                return true;
            return false; 
        }


        private void gópÝVềPhầnMềmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEmailFeedBack formEmailFeedBack = new FormEmailFeedBack();
            formEmailFeedBack.ShowDialog();
        }

        private void timerAdminOnlineHour_Tick(object sender, EventArgs e)
        {
            FormDangNhap.admin.AdminOnlineHour++;
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có thực sự muốn thoát", "THÔNG BÁO", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dialogResult != DialogResult.OK)
            {
                e.Cancel = true;
            }
            else
            {
                timerAdminOnlineHour.Stop();
                MessageBox.Show("Thời gian online: " + FormDangNhap.admin.AdminOnlineHour.ToString());
            }

        }

        

        private void GetAdminInfo()
        {
            textBoxAdminId.Text = FormDangNhap.admin.AdminId;
            textBoxAdminName.Text = FormDangNhap.admin.AdminName;
            if(FormDangNhap.admin.AdminType==1)
            {
                textBoxAdminJob.Text = "Quản trị viên";
            }
            else
            {
                textBoxAdminJob.Text = "Nhân viên";
            }
            textBoxAdminAddress.Text = FormDangNhap.admin.AdminAddress;
            textBoxAdminEmail.Text = FormDangNhap.admin.AdminEmail;
            dateTimePickerAdminBirth.Text = (FormDangNhap.admin.AdminBirth).ToString();
            labelAdminOnlineHour.Text = (FormDangNhap.admin.AdminOnlineHour/60).ToString();

            textBoxAdminId.ReadOnly = true;
            textBoxAdminName.ReadOnly = true;
            textBoxAdminJob.ReadOnly = true;
            textBoxAdminAddress.ReadOnly = true;
            textBoxAdminEmail.ReadOnly = true;
            dateTimePickerAdminBirth.Enabled = false;

        }

        private int UpdateAdminProfile(string name,string address, string email,string birth)
        {
            FormDangNhap.admin.AdminName = name;
            FormDangNhap.admin.AdminAddress = address;
            FormDangNhap.admin.AdminBirth = Convert.ToDateTime(birth);
            FormDangNhap.admin.AdminEmail = textBoxAdminEmail.Text;
            FormDangNhap.admin.AdminAddress = textBoxAdminAddress.Text;
            return bal_Admin.UpdateAdminProfile(FormDangNhap.admin);
        }

        private void cậpNhậtHồSơToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxAdminName.ReadOnly = false;
            textBoxAdminAddress.ReadOnly = false;
            textBoxAdminEmail.ReadOnly = false;
            dateTimePickerAdminBirth.Enabled = true;
            buttonUpdateProfile.Visible = true;
            buttonUpdateProfile.Enabled = true;
        }

       

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdateProfile_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(textBoxAdminName.Text) || string.IsNullOrEmpty(dateTimePickerAdminBirth.Text))
            {
                MessageBox.Show("Vui lòng điền đầy đủ thông tin", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (string.IsNullOrEmpty(textBoxAdminName.Text))
                    textBoxAdminName.Focus();
                else
                    dateTimePickerAdminBirth.Focus();
            }
            else
            {
                try
                {
                    UpdateAdminProfile(textBoxAdminName.Text, textBoxAdminAddress.Text, textBoxAdminEmail.Text, dateTimePickerAdminBirth.Text);
                    MessageBox.Show("Cập nhật thành công","THÔNG BÁO",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    buttonUpdateProfile.Visible = false;
                    buttonUpdateProfile.Enabled = false;
                    FormMain_Load(sender, e);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            } 
    }

        private void thôngTinCáNhânToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormAdminTable formAdminTable = new FormAdminTable();
            this.Hide();
            formAdminTable.ShowDialog();
            this.Show();

        }

        private int ChangeAdminPassword(string id, string oldPassword, string newPassword)
        {
            return bal_Admin.UpdateAdminPassWord(id, oldPassword, newPassword);
        }

        private void đổiMậtKhẩuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormChangeAdminPassword formChangeAdminPassword = new FormChangeAdminPassword();
            formChangeAdminPassword.myDelegate = new FormChangeAdminPassword.MyDelegate(ChangeAdminPassword);
            formChangeAdminPassword.ShowDialog();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            FormMain_Load(sender, e);  
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            timerAdminOnlineHour.Start();
            if (CheckTypeOfAdmin() != false)
            {
                thôngTinCáNhânToolStripMenuItem1.Visible = true;
                thôngTinCáNhânToolStripMenuItem1.Enabled = true;
            }
            thôngTinCáNhânToolStripMenuItem.Text = "Xin chào " + GetAdminName();
            GetAdminInfo();
            buttonUpdateProfile.Visible = false;
            buttonUpdateProfile.Enabled = false;
        }
    }
}
