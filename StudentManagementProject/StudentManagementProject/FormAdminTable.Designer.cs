﻿namespace StudentManagementProject
{
    partial class FormAdminTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBoxQueryToDataBase = new System.Windows.Forms.GroupBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonCancelSearch = new System.Windows.Forms.Button();
            this.labelSearching = new System.Windows.Forms.Label();
            this.textBoxSearching = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonXóa = new System.Windows.Forms.Button();
            this.buttonAddAdmin = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBoxAdminInfo = new System.Windows.Forms.GroupBox();
            this.numericUpDownAdminType = new System.Windows.Forms.NumericUpDown();
            this.textBoxAdminEmail = new System.Windows.Forms.TextBox();
            this.textBoxAdminAddress = new System.Windows.Forms.TextBox();
            this.dateTimePickerAdminBirth = new System.Windows.Forms.DateTimePicker();
            this.textBoxAdminPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxAdminName = new System.Windows.Forms.TextBox();
            this.textBoxAdminId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewAdminInfo = new System.Windows.Forms.DataGridView();
            this.adminId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminOnlineHour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBoxQueryToDataBase.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBoxAdminInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAdminType)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdminInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 458);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel5.Controls.Add(this.label8);
            this.panel5.Location = new System.Drawing.Point(4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1071, 77);
            this.panel5.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(389, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(229, 25);
            this.label8.TabIndex = 0;
            this.label8.Text = "QUẢN LÝ TÀI KHOẢN ";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBoxQueryToDataBase);
            this.panel4.Location = new System.Drawing.Point(730, 346);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(345, 112);
            this.panel4.TabIndex = 2;
            // 
            // groupBoxQueryToDataBase
            // 
            this.groupBoxQueryToDataBase.Controls.Add(this.buttonRefresh);
            this.groupBoxQueryToDataBase.Controls.Add(this.buttonCancelSearch);
            this.groupBoxQueryToDataBase.Controls.Add(this.labelSearching);
            this.groupBoxQueryToDataBase.Controls.Add(this.textBoxSearching);
            this.groupBoxQueryToDataBase.Controls.Add(this.buttonSearch);
            this.groupBoxQueryToDataBase.Controls.Add(this.buttonXóa);
            this.groupBoxQueryToDataBase.Controls.Add(this.buttonAddAdmin);
            this.groupBoxQueryToDataBase.Location = new System.Drawing.Point(3, 14);
            this.groupBoxQueryToDataBase.Name = "groupBoxQueryToDataBase";
            this.groupBoxQueryToDataBase.Size = new System.Drawing.Size(339, 92);
            this.groupBoxQueryToDataBase.TabIndex = 0;
            this.groupBoxQueryToDataBase.TabStop = false;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackgroundImage = global::StudentManagementProject.Properties.Resources.refresh;
            this.buttonRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonRefresh.Location = new System.Drawing.Point(9, 48);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(47, 33);
            this.buttonRefresh.TabIndex = 13;
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonCancelSearch
            // 
            this.buttonCancelSearch.Enabled = false;
            this.buttonCancelSearch.Location = new System.Drawing.Point(254, 19);
            this.buttonCancelSearch.Name = "buttonCancelSearch";
            this.buttonCancelSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelSearch.TabIndex = 12;
            this.buttonCancelSearch.Text = "Xong";
            this.buttonCancelSearch.UseVisualStyleBackColor = true;
            this.buttonCancelSearch.Visible = false;
            this.buttonCancelSearch.Click += new System.EventHandler(this.buttonCancelSearch_Click);
            // 
            // labelSearching
            // 
            this.labelSearching.AutoSize = true;
            this.labelSearching.Enabled = false;
            this.labelSearching.Location = new System.Drawing.Point(6, 6);
            this.labelSearching.Name = "labelSearching";
            this.labelSearching.Size = new System.Drawing.Size(128, 13);
            this.labelSearching.TabIndex = 9;
            this.labelSearching.Text = "Từ khóa theo Id hoặc tên";
            this.labelSearching.Visible = false;
            // 
            // textBoxSearching
            // 
            this.textBoxSearching.Enabled = false;
            this.textBoxSearching.Location = new System.Drawing.Point(9, 22);
            this.textBoxSearching.Name = "textBoxSearching";
            this.textBoxSearching.Size = new System.Drawing.Size(239, 20);
            this.textBoxSearching.TabIndex = 8;
            this.textBoxSearching.Visible = false;
            this.textBoxSearching.TextChanged += new System.EventHandler(this.textBoxSearching_TextChanged);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(254, 48);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 31);
            this.buttonSearch.TabIndex = 7;
            this.buttonSearch.Text = "Tìm kiếm";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonXóa
            // 
            this.buttonXóa.Location = new System.Drawing.Point(173, 48);
            this.buttonXóa.Name = "buttonXóa";
            this.buttonXóa.Size = new System.Drawing.Size(75, 31);
            this.buttonXóa.TabIndex = 6;
            this.buttonXóa.Text = "Xóa";
            this.buttonXóa.UseVisualStyleBackColor = true;
            this.buttonXóa.Click += new System.EventHandler(this.buttonXóa_Click);
            // 
            // buttonAddAdmin
            // 
            this.buttonAddAdmin.Location = new System.Drawing.Point(92, 48);
            this.buttonAddAdmin.Name = "buttonAddAdmin";
            this.buttonAddAdmin.Size = new System.Drawing.Size(75, 31);
            this.buttonAddAdmin.TabIndex = 5;
            this.buttonAddAdmin.Text = "Thêm";
            this.buttonAddAdmin.UseVisualStyleBackColor = true;
            this.buttonAddAdmin.Click += new System.EventHandler(this.buttonAddAdmin_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBoxAdminInfo);
            this.panel3.Location = new System.Drawing.Point(730, 87);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(345, 253);
            this.panel3.TabIndex = 1;
            // 
            // groupBoxAdminInfo
            // 
            this.groupBoxAdminInfo.Controls.Add(this.numericUpDownAdminType);
            this.groupBoxAdminInfo.Controls.Add(this.textBoxAdminEmail);
            this.groupBoxAdminInfo.Controls.Add(this.textBoxAdminAddress);
            this.groupBoxAdminInfo.Controls.Add(this.dateTimePickerAdminBirth);
            this.groupBoxAdminInfo.Controls.Add(this.textBoxAdminPassword);
            this.groupBoxAdminInfo.Controls.Add(this.label7);
            this.groupBoxAdminInfo.Controls.Add(this.textBoxAdminName);
            this.groupBoxAdminInfo.Controls.Add(this.textBoxAdminId);
            this.groupBoxAdminInfo.Controls.Add(this.label6);
            this.groupBoxAdminInfo.Controls.Add(this.label5);
            this.groupBoxAdminInfo.Controls.Add(this.label4);
            this.groupBoxAdminInfo.Controls.Add(this.label3);
            this.groupBoxAdminInfo.Controls.Add(this.label2);
            this.groupBoxAdminInfo.Controls.Add(this.label1);
            this.groupBoxAdminInfo.Enabled = false;
            this.groupBoxAdminInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxAdminInfo.Location = new System.Drawing.Point(3, 10);
            this.groupBoxAdminInfo.Name = "groupBoxAdminInfo";
            this.groupBoxAdminInfo.Size = new System.Drawing.Size(335, 239);
            this.groupBoxAdminInfo.TabIndex = 0;
            this.groupBoxAdminInfo.TabStop = false;
            this.groupBoxAdminInfo.Text = "Thông tin tài khoản";
            this.groupBoxAdminInfo.Visible = false;
            // 
            // numericUpDownAdminType
            // 
            this.numericUpDownAdminType.Location = new System.Drawing.Point(210, 125);
            this.numericUpDownAdminType.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownAdminType.Name = "numericUpDownAdminType";
            this.numericUpDownAdminType.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownAdminType.TabIndex = 15;
            // 
            // textBoxAdminEmail
            // 
            this.textBoxAdminEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdminEmail.Location = new System.Drawing.Point(94, 209);
            this.textBoxAdminEmail.Name = "textBoxAdminEmail";
            this.textBoxAdminEmail.Size = new System.Drawing.Size(236, 22);
            this.textBoxAdminEmail.TabIndex = 14;
            // 
            // textBoxAdminAddress
            // 
            this.textBoxAdminAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdminAddress.Location = new System.Drawing.Point(94, 181);
            this.textBoxAdminAddress.Name = "textBoxAdminAddress";
            this.textBoxAdminAddress.Size = new System.Drawing.Size(235, 22);
            this.textBoxAdminAddress.TabIndex = 13;
            // 
            // dateTimePickerAdminBirth
            // 
            this.dateTimePickerAdminBirth.Location = new System.Drawing.Point(94, 153);
            this.dateTimePickerAdminBirth.Name = "dateTimePickerAdminBirth";
            this.dateTimePickerAdminBirth.Size = new System.Drawing.Size(236, 22);
            this.dateTimePickerAdminBirth.TabIndex = 12;
            // 
            // textBoxAdminPassword
            // 
            this.textBoxAdminPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdminPassword.Location = new System.Drawing.Point(95, 94);
            this.textBoxAdminPassword.Name = "textBoxAdminPassword";
            this.textBoxAdminPassword.Size = new System.Drawing.Size(235, 22);
            this.textBoxAdminPassword.TabIndex = 10;
            this.textBoxAdminPassword.UseSystemPasswordChar = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 16);
            this.label7.TabIndex = 9;
            this.label7.Text = "Mật khẩu:";
            // 
            // textBoxAdminName
            // 
            this.textBoxAdminName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdminName.Location = new System.Drawing.Point(94, 61);
            this.textBoxAdminName.Name = "textBoxAdminName";
            this.textBoxAdminName.Size = new System.Drawing.Size(236, 22);
            this.textBoxAdminName.TabIndex = 8;
            // 
            // textBoxAdminId
            // 
            this.textBoxAdminId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdminId.Location = new System.Drawing.Point(94, 30);
            this.textBoxAdminId.Name = "textBoxAdminId";
            this.textBoxAdminId.Size = new System.Drawing.Size(235, 22);
            this.textBoxAdminId.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ngày sinh:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Chức vụ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Họ tên:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Id:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewAdminInfo);
            this.panel2.Location = new System.Drawing.Point(4, 87);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(720, 368);
            this.panel2.TabIndex = 0;
            // 
            // dataGridViewAdminInfo
            // 
            this.dataGridViewAdminInfo.AllowUserToAddRows = false;
            this.dataGridViewAdminInfo.AllowUserToDeleteRows = false;
            this.dataGridViewAdminInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAdminInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.adminId,
            this.adminName,
            this.adminType,
            this.adminBirth,
            this.adminAddress,
            this.adminEmail,
            this.adminOnlineHour});
            this.dataGridViewAdminInfo.Location = new System.Drawing.Point(2, 10);
            this.dataGridViewAdminInfo.Name = "dataGridViewAdminInfo";
            this.dataGridViewAdminInfo.ReadOnly = true;
            this.dataGridViewAdminInfo.Size = new System.Drawing.Size(715, 355);
            this.dataGridViewAdminInfo.TabIndex = 0;
            // 
            // adminId
            // 
            this.adminId.DataPropertyName = "adminId";
            this.adminId.HeaderText = "Id";
            this.adminId.Name = "adminId";
            this.adminId.ReadOnly = true;
            this.adminId.Width = 75;
            // 
            // adminName
            // 
            this.adminName.DataPropertyName = "adminName";
            this.adminName.HeaderText = "Họ tên";
            this.adminName.Name = "adminName";
            this.adminName.ReadOnly = true;
            // 
            // adminType
            // 
            this.adminType.DataPropertyName = "adminType";
            this.adminType.HeaderText = "Chức vụ";
            this.adminType.Name = "adminType";
            this.adminType.ReadOnly = true;
            // 
            // adminBirth
            // 
            this.adminBirth.DataPropertyName = "adminBirth";
            this.adminBirth.HeaderText = "Ngày sinh";
            this.adminBirth.Name = "adminBirth";
            this.adminBirth.ReadOnly = true;
            // 
            // adminAddress
            // 
            this.adminAddress.DataPropertyName = "adminAddress";
            this.adminAddress.HeaderText = "Địa chỉ";
            this.adminAddress.Name = "adminAddress";
            this.adminAddress.ReadOnly = true;
            // 
            // adminEmail
            // 
            this.adminEmail.DataPropertyName = "adminEmail";
            this.adminEmail.HeaderText = "Email";
            this.adminEmail.Name = "adminEmail";
            this.adminEmail.ReadOnly = true;
            // 
            // adminOnlineHour
            // 
            this.adminOnlineHour.DataPropertyName = "adminOnlineHour";
            this.adminOnlineHour.HeaderText = "Số giờ làm việc";
            this.adminOnlineHour.Name = "adminOnlineHour";
            this.adminOnlineHour.ReadOnly = true;
            // 
            // FormAdminTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 483);
            this.Controls.Add(this.panel1);
            this.Name = "FormAdminTable";
            this.Text = "DANH SÁCH TÀI KHOẢN QUẢN TRỊ";
            this.Load += new System.EventHandler(this.FormAdminTable_Load);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.groupBoxQueryToDataBase.ResumeLayout(false);
            this.groupBoxQueryToDataBase.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBoxAdminInfo.ResumeLayout(false);
            this.groupBoxAdminInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAdminType)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdminInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewAdminInfo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBoxAdminInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn adminId;
        private System.Windows.Forms.DataGridViewTextBoxColumn adminName;
        private System.Windows.Forms.DataGridViewTextBoxColumn adminType;
        private System.Windows.Forms.DataGridViewTextBoxColumn adminBirth;
        private System.Windows.Forms.DataGridViewTextBoxColumn adminAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn adminEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn adminOnlineHour;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBoxQueryToDataBase;
        private System.Windows.Forms.Label labelSearching;
        private System.Windows.Forms.TextBox textBoxSearching;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonXóa;
        private System.Windows.Forms.Button buttonAddAdmin;
        private System.Windows.Forms.TextBox textBoxAdminPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxAdminName;
        private System.Windows.Forms.TextBox textBoxAdminId;
        private System.Windows.Forms.Button buttonCancelSearch;
        private System.Windows.Forms.TextBox textBoxAdminEmail;
        private System.Windows.Forms.TextBox textBoxAdminAddress;
        private System.Windows.Forms.DateTimePicker dateTimePickerAdminBirth;
        private System.Windows.Forms.NumericUpDown numericUpDownAdminType;
        private System.Windows.Forms.Button buttonRefresh;
    }
}