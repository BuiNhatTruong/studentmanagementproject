﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DatabaseAccessLayer;
using ValueObject;
using System.Data.SqlClient;
using BussinessAccessLayer;
namespace StudentManagementProject
{
    public partial class FormAdminTable : Form
    {
        public FormAdminTable()
        {
            InitializeComponent();
        }

        BAL_Admin BAL_Admin = new BAL_Admin();

        public DataTable GetAllAdminInfo()
        {
            return BAL_Admin.GetAllAdminInfo();
        }

        private void FormAdminTable_Load(object sender, EventArgs e)
        {
            dataGridViewAdminInfo.DataSource=GetAllAdminInfo();
            
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            labelSearching.Visible = true;
            labelSearching.Enabled = true;
            textBoxSearching.Visible = true;
            textBoxSearching.Enabled = true;
            buttonCancelSearch.Visible = true;
            buttonCancelSearch.Enabled = true;
        }

        private void textBoxSearching_TextChanged(object sender, EventArgs e)
        {
            dataGridViewAdminInfo.DataSource=BAL_Admin.SearchingAdminInfo(textBoxSearching.Text);
        }

        private void buttonCancelSearch_Click(object sender, EventArgs e)
        {
            labelSearching.Visible = false;
            textBoxSearching.Visible = false;
            textBoxSearching.Enabled = false;
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            FormAdminTable_Load(sender, e);
        }

        private bool checkInformation()
        {
            if (string.IsNullOrEmpty(textBoxAdminId.Text)
                    || string.IsNullOrEmpty(textBoxAdminName.Text)
                    || string.IsNullOrEmpty(textBoxAdminPassword.Text)
                    || string.IsNullOrEmpty(numericUpDownAdminType.Value.ToString())
                    || string.IsNullOrEmpty(dateTimePickerAdminBirth.Text)
                )
            {
                return false;
            }
            return true;
        }

        private void buttonAddAdmin_Click(object sender, EventArgs e)
        {
            bool infoChecked = checkInformation();
            if(infoChecked==false)
            {
                groupBoxAdminInfo.Visible = true;
                groupBoxAdminInfo.Enabled = true;
                MessageBox.Show("Vui lòng điền đầy đủ thông tin tài khoản !","THÔNG BÁO",MessageBoxButtons.OK,MessageBoxIcon.Information);
                textBoxAdminId.Focus();
            }
            else
            {
                try
                {
                    DateTime adminBirth = Convert.ToDateTime(dateTimePickerAdminBirth.Text);
                    Admin newAdmin = new Admin(textBoxAdminId.Text, textBoxAdminPassword.Text, textBoxAdminName.Text, (int)numericUpDownAdminType.Value, adminBirth, textBoxAdminAddress.Text, textBoxAdminEmail.Text, 0);
                    BAL_Admin.AddAdmin(newAdmin);
                    MessageBox.Show("Thao tác thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    groupBoxAdminInfo.Visible = false;
                    groupBoxAdminInfo.Enabled = false;
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    groupBoxAdminInfo.Visible =false;
                    groupBoxAdminInfo.Enabled = false;
                    textBoxAdminId.Text = null;
                    textBoxAdminName.Text = null;
                    textBoxAdminPassword.Text = null;
                    numericUpDownAdminType.Value = 0;
                    dateTimePickerAdminBirth.Value = DateTime.Now;
                }
            }
            
        }

        private void buttonXóa_Click(object sender, EventArgs e)
        {
            try
            {
                Admin deleteAdmin = new Admin();
                deleteAdmin.AdminId = dataGridViewAdminInfo.SelectedCells[0].Value.ToString();
                BAL_Admin.DeleteAdmin(deleteAdmin);
                MessageBox.Show("Đã xóa tài khoản!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
