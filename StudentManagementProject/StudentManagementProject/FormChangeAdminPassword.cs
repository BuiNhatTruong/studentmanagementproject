﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementProject
{
    public partial class FormChangeAdminPassword : Form
    {
        public FormChangeAdminPassword()
        {
            InitializeComponent();
            textBoxAdminId.Text = FormDangNhap.admin.AdminId;
        }

        public delegate int MyDelegate(string id, string oldPassword, string newPassword);
        public MyDelegate myDelegate;

        private void buttonCancelUpdate_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonChangePassword_Click(object sender, EventArgs e)
        {
            myDelegate(textBoxAdminId.Text, textBoxOldPassword.Text, textBoxNewPassword.Text);
            MessageBox.Show("Cập nhật thành công");
        }
    }
}
