﻿namespace StudentManagementProject
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.thôngTinCáNhânToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngTinCáNhânToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.đổiMậtKhẩuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cậpNhậtHồSơToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSáchSinhViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSáchHọcPhầnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSáchLớpHọcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSáchGiáoViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trưngCầuÝKiếnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gópÝVềPhầnMềmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerAdminOnlineHour = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonUpdateProfile = new System.Windows.Forms.Button();
            this.labelAdminOnlineHour = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonLogOut = new System.Windows.Forms.Button();
            this.dateTimePickerAdminBirth = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAdminEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAdminJob = new System.Windows.Forms.TextBox();
            this.textBoxAdminAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAdminName = new System.Windows.Forms.TextBox();
            this.textBoxAdminId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBoxLoginImg = new System.Windows.Forms.PictureBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.menuStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoginImg)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thôngTinCáNhânToolStripMenuItem,
            this.danhSáchSinhViênToolStripMenuItem,
            this.danhSáchHọcPhầnToolStripMenuItem,
            this.danhSáchLớpHọcToolStripMenuItem,
            this.danhSáchGiáoViênToolStripMenuItem,
            this.trưngCầuÝKiếnToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(889, 27);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // thôngTinCáNhânToolStripMenuItem
            // 
            this.thôngTinCáNhânToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thôngTinCáNhânToolStripMenuItem1,
            this.đổiMậtKhẩuToolStripMenuItem,
            this.cậpNhậtHồSơToolStripMenuItem});
            this.thôngTinCáNhânToolStripMenuItem.Name = "thôngTinCáNhânToolStripMenuItem";
            this.thôngTinCáNhânToolStripMenuItem.Size = new System.Drawing.Size(12, 23);
            // 
            // thôngTinCáNhânToolStripMenuItem1
            // 
            this.thôngTinCáNhânToolStripMenuItem1.Enabled = false;
            this.thôngTinCáNhânToolStripMenuItem1.Name = "thôngTinCáNhânToolStripMenuItem1";
            this.thôngTinCáNhânToolStripMenuItem1.Size = new System.Drawing.Size(193, 24);
            this.thôngTinCáNhânToolStripMenuItem1.Text = "Thông tin tài khoản";
            this.thôngTinCáNhânToolStripMenuItem1.Visible = false;
            this.thôngTinCáNhânToolStripMenuItem1.Click += new System.EventHandler(this.thôngTinCáNhânToolStripMenuItem1_Click);
            // 
            // đổiMậtKhẩuToolStripMenuItem
            // 
            this.đổiMậtKhẩuToolStripMenuItem.Name = "đổiMậtKhẩuToolStripMenuItem";
            this.đổiMậtKhẩuToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.đổiMậtKhẩuToolStripMenuItem.Text = "Đổi mật khẩu";
            this.đổiMậtKhẩuToolStripMenuItem.Click += new System.EventHandler(this.đổiMậtKhẩuToolStripMenuItem_Click);
            // 
            // cậpNhậtHồSơToolStripMenuItem
            // 
            this.cậpNhậtHồSơToolStripMenuItem.Name = "cậpNhậtHồSơToolStripMenuItem";
            this.cậpNhậtHồSơToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.cậpNhậtHồSơToolStripMenuItem.Text = "Cập nhật hồ sơ";
            this.cậpNhậtHồSơToolStripMenuItem.Click += new System.EventHandler(this.cậpNhậtHồSơToolStripMenuItem_Click);
            // 
            // danhSáchSinhViênToolStripMenuItem
            // 
            this.danhSáchSinhViênToolStripMenuItem.Name = "danhSáchSinhViênToolStripMenuItem";
            this.danhSáchSinhViênToolStripMenuItem.Size = new System.Drawing.Size(139, 23);
            this.danhSáchSinhViênToolStripMenuItem.Text = "Danh sách sinh viên";
            // 
            // danhSáchHọcPhầnToolStripMenuItem
            // 
            this.danhSáchHọcPhầnToolStripMenuItem.Name = "danhSáchHọcPhầnToolStripMenuItem";
            this.danhSáchHọcPhầnToolStripMenuItem.Size = new System.Drawing.Size(143, 23);
            this.danhSáchHọcPhầnToolStripMenuItem.Text = "Danh sách học phần";
            // 
            // danhSáchLớpHọcToolStripMenuItem
            // 
            this.danhSáchLớpHọcToolStripMenuItem.Name = "danhSáchLớpHọcToolStripMenuItem";
            this.danhSáchLớpHọcToolStripMenuItem.Size = new System.Drawing.Size(133, 23);
            this.danhSáchLớpHọcToolStripMenuItem.Text = "Danh sách lớp học";
            // 
            // danhSáchGiáoViênToolStripMenuItem
            // 
            this.danhSáchGiáoViênToolStripMenuItem.Name = "danhSáchGiáoViênToolStripMenuItem";
            this.danhSáchGiáoViênToolStripMenuItem.Size = new System.Drawing.Size(141, 23);
            this.danhSáchGiáoViênToolStripMenuItem.Text = "Danh sách giáo viên";
            // 
            // trưngCầuÝKiếnToolStripMenuItem
            // 
            this.trưngCầuÝKiếnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gópÝVềPhầnMềmToolStripMenuItem});
            this.trưngCầuÝKiếnToolStripMenuItem.Name = "trưngCầuÝKiếnToolStripMenuItem";
            this.trưngCầuÝKiếnToolStripMenuItem.Size = new System.Drawing.Size(122, 23);
            this.trưngCầuÝKiếnToolStripMenuItem.Text = "Trưng cầu ý kiến";
            // 
            // gópÝVềPhầnMềmToolStripMenuItem
            // 
            this.gópÝVềPhầnMềmToolStripMenuItem.Name = "gópÝVềPhầnMềmToolStripMenuItem";
            this.gópÝVềPhầnMềmToolStripMenuItem.Size = new System.Drawing.Size(200, 24);
            this.gópÝVềPhầnMềmToolStripMenuItem.Text = "Góp ý về phần mềm";
            this.gópÝVềPhầnMềmToolStripMenuItem.Click += new System.EventHandler(this.gópÝVềPhầnMềmToolStripMenuItem_Click);
            // 
            // timerAdminOnlineHour
            // 
            this.timerAdminOnlineHour.Interval = 1000;
            this.timerAdminOnlineHour.Tick += new System.EventHandler(this.timerAdminOnlineHour_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(12, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(440, 326);
            this.panel1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonRefresh);
            this.groupBox1.Controls.Add(this.buttonUpdateProfile);
            this.groupBox1.Controls.Add(this.labelAdminOnlineHour);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.buttonLogOut);
            this.groupBox1.Controls.Add(this.dateTimePickerAdminBirth);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxAdminEmail);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxAdminJob);
            this.groupBox1.Controls.Add(this.textBoxAdminAddress);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxAdminName);
            this.groupBox1.Controls.Add(this.textBoxAdminId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 312);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin tài khoản";
            // 
            // buttonUpdateProfile
            // 
            this.buttonUpdateProfile.Enabled = false;
            this.buttonUpdateProfile.Location = new System.Drawing.Point(190, 273);
            this.buttonUpdateProfile.Name = "buttonUpdateProfile";
            this.buttonUpdateProfile.Size = new System.Drawing.Size(108, 33);
            this.buttonUpdateProfile.TabIndex = 25;
            this.buttonUpdateProfile.Text = "Cập nhật";
            this.buttonUpdateProfile.UseVisualStyleBackColor = true;
            this.buttonUpdateProfile.Visible = false;
            this.buttonUpdateProfile.Click += new System.EventHandler(this.buttonUpdateProfile_Click);
            // 
            // labelAdminOnlineHour
            // 
            this.labelAdminOnlineHour.AutoSize = true;
            this.labelAdminOnlineHour.Location = new System.Drawing.Point(380, 230);
            this.labelAdminOnlineHour.Name = "labelAdminOnlineHour";
            this.labelAdminOnlineHour.Size = new System.Drawing.Size(0, 19);
            this.labelAdminOnlineHour.TabIndex = 24;
            this.labelAdminOnlineHour.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 19);
            this.label7.TabIndex = 23;
            this.label7.Text = "♦ Số giờ làm việc:";
            // 
            // buttonLogOut
            // 
            this.buttonLogOut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonLogOut.Location = new System.Drawing.Point(304, 273);
            this.buttonLogOut.Name = "buttonLogOut";
            this.buttonLogOut.Size = new System.Drawing.Size(121, 33);
            this.buttonLogOut.TabIndex = 22;
            this.buttonLogOut.Text = "Đăng xuất";
            this.buttonLogOut.UseVisualStyleBackColor = true;
            this.buttonLogOut.Click += new System.EventHandler(this.buttonLogOut_Click);
            // 
            // dateTimePickerAdminBirth
            // 
            this.dateTimePickerAdminBirth.Enabled = false;
            this.dateTimePickerAdminBirth.Location = new System.Drawing.Point(190, 99);
            this.dateTimePickerAdminBirth.Name = "dateTimePickerAdminBirth";
            this.dateTimePickerAdminBirth.Size = new System.Drawing.Size(235, 26);
            this.dateTimePickerAdminBirth.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 19);
            this.label6.TabIndex = 20;
            this.label6.Text = "♦ Ngày sinh:";
            // 
            // textBoxAdminEmail
            // 
            this.textBoxAdminEmail.Location = new System.Drawing.Point(190, 196);
            this.textBoxAdminEmail.Name = "textBoxAdminEmail";
            this.textBoxAdminEmail.ReadOnly = true;
            this.textBoxAdminEmail.Size = new System.Drawing.Size(235, 26);
            this.textBoxAdminEmail.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 19);
            this.label5.TabIndex = 18;
            this.label5.Text = "♦ Email:";
            // 
            // textBoxAdminJob
            // 
            this.textBoxAdminJob.Location = new System.Drawing.Point(190, 135);
            this.textBoxAdminJob.Name = "textBoxAdminJob";
            this.textBoxAdminJob.ReadOnly = true;
            this.textBoxAdminJob.Size = new System.Drawing.Size(235, 26);
            this.textBoxAdminJob.TabIndex = 17;
            // 
            // textBoxAdminAddress
            // 
            this.textBoxAdminAddress.Location = new System.Drawing.Point(190, 164);
            this.textBoxAdminAddress.Name = "textBoxAdminAddress";
            this.textBoxAdminAddress.ReadOnly = true;
            this.textBoxAdminAddress.Size = new System.Drawing.Size(235, 26);
            this.textBoxAdminAddress.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 19);
            this.label4.TabIndex = 15;
            this.label4.Text = "♦ Địa chỉ:";
            // 
            // textBoxAdminName
            // 
            this.textBoxAdminName.Location = new System.Drawing.Point(190, 67);
            this.textBoxAdminName.Name = "textBoxAdminName";
            this.textBoxAdminName.ReadOnly = true;
            this.textBoxAdminName.Size = new System.Drawing.Size(235, 26);
            this.textBoxAdminName.TabIndex = 14;
            // 
            // textBoxAdminId
            // 
            this.textBoxAdminId.Location = new System.Drawing.Point(190, 38);
            this.textBoxAdminId.Name = "textBoxAdminId";
            this.textBoxAdminId.ReadOnly = true;
            this.textBoxAdminId.Size = new System.Drawing.Size(235, 26);
            this.textBoxAdminId.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 12;
            this.label3.Text = "♦ Chức vụ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "♦ Họ tên quản trị viên:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 19);
            this.label1.TabIndex = 10;
            this.label1.Text = "♦ Mã quản trị viên:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBoxLoginImg);
            this.panel2.Location = new System.Drawing.Point(459, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(418, 326);
            this.panel2.TabIndex = 2;
            // 
            // pictureBoxLoginImg
            // 
            this.pictureBoxLoginImg.Image = global::StudentManagementProject.Properties.Resources.UserInfo;
            this.pictureBoxLoginImg.Location = new System.Drawing.Point(3, 11);
            this.pictureBoxLoginImg.Name = "pictureBoxLoginImg";
            this.pictureBoxLoginImg.Size = new System.Drawing.Size(412, 312);
            this.pictureBoxLoginImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLoginImg.TabIndex = 0;
            this.pictureBoxLoginImg.TabStop = false;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonRefresh.BackgroundImage = global::StudentManagementProject.Properties.Resources.refresh;
            this.buttonRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonRefresh.Location = new System.Drawing.Point(20, 256);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(50, 50);
            this.buttonRefresh.TabIndex = 26;
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonLogOut;
            this.ClientSize = new System.Drawing.Size(889, 375);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font(".VnArial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PHẦN MỀM QUẢN LÝ SINH VIÊN";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoginImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem thôngTinCáNhânToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSáchSinhViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSáchHọcPhầnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSáchLớpHọcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSáchGiáoViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thôngTinCáNhânToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem đổiMậtKhẩuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cậpNhậtHồSơToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trưngCầuÝKiếnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gópÝVềPhầnMềmToolStripMenuItem;
        private System.Windows.Forms.Timer timerAdminOnlineHour;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxAdminEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAdminJob;
        private System.Windows.Forms.TextBox textBoxAdminAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAdminName;
        private System.Windows.Forms.TextBox textBoxAdminId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBoxLoginImg;
        private System.Windows.Forms.DateTimePicker dateTimePickerAdminBirth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonLogOut;
        private System.Windows.Forms.Label labelAdminOnlineHour;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonUpdateProfile;
        private System.Windows.Forms.Button buttonRefresh;
    }
}